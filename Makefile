all:
	pdflatex Thesis.tex
	bibtex Thesis.aux
	pdflatex Thesis.tex
	pdflatex Thesis.tex

clean:
	rm *~ Thesis.pdf Thesis.aux Thesis.bbl Thesis.blg Thesis.glo Thesis.ist Thesis.lof Thesis.lot Thesis.log Thesis.out Thesis.toc Chapters/*aux Chapters/*~ FrontBackmatter/*aux FrontBackmatter/*~
