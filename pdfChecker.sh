#!/bin/bash

# This scripts checks how many pages have to be printed colored.

FILE=$1
PAGES=$(pdfinfo ${FILE} | grep 'Pages:' | sed 's/Pages:\s*//')

GRAYPAGES=""
COLORPAGES=""
DOUBLECOLORPAGES=""
LAST=""
CDP=0
GDP=0
CP=0
GP=0

echo "Pages: $PAGES"
N=1
COLORSPACE=""
while (test "$N" -le "$PAGES")
do
    LAST=$COLORSPACE
    COLORSPACE=$( identify -format "%[colorspace]" "$FILE[$((N-1))]" )
    echo "$N: $COLORSPACE"
    if [[ $COLORSPACE == "Gray" ]]
    then
        GRAYPAGES="$GRAYPAGES $N"
        GP=$((GP+1))
    else
        COLORPAGES="$COLORPAGES $N"
        CP=$((CP+1))
        # For double sided documents also list the page on the other side of the sheet:
    fi
    if [[ $((N%2)) -eq 0 ]]
    then
        if [[ $LAST == "Gray" && $COLORSPACE == "Gray" ]]
        then
            DOUBLEGRAYPAGES="$DOUBLEGRAYPAGES $((N-1)) $N"
            GDP=$((GDP+2))
        else
            DOUBLECOLORPAGES="$DOUBLECOLORPAGES $((N-1)) $N"
            CDP=$((CDP+2))
        fi
    fi
    N=$((N+1))
done


echo "SingleColored ($CP): $COLORPAGES"
echo "SingleGray ($GP): $GRAYPAGES"
echo "DoubleColored ($CDP): $DOUBLECOLORPAGES"
echo "DoubleGray ($GDP): $DOUBLEGRAYPAGES"
#pdftk $FILE cat $COLORPAGES output color_${FILE}.pdf